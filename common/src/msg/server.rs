use super::{ClientState, EcsCompPacket, EcsResPacket};
use crate::{
    comp,
    terrain::{Block, TerrainChunk},
};
use fxhash::FxHashMap;
use vek::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum RequestStateError {
    Denied,
    Already,
    Impossible,
    WrongMessage,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServerInfo {
    pub name: String,
    pub description: String,
    pub git_hash: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ServerMsg {
    InitialSync {
        ecs_state: sphynx::StatePackage<EcsCompPacket, EcsResPacket>,
        entity_uid: u64,
        server_info: ServerInfo,
    },
    StateAnswer(Result<ClientState, (RequestStateError, ClientState)>),
    ForceState(ClientState),
    Ping,
    Pong,
    Chat(String),
    SetPlayerEntity(u64),
    EcsSync(sphynx::SyncPackage<EcsCompPacket, EcsResPacket>),
    EntityPhysics {
        entity: u64,
        pos: comp::Pos,
        vel: comp::Vel,
        ori: comp::Ori,
        action_state: Option<comp::ActionState>,
    },
    TerrainChunkUpdate {
        key: Vec2<i32>,
        chunk: Box<TerrainChunk>,
    },
    TerrainBlockUpdates(FxHashMap<Vec3<i32>, Block>),
    Error(ServerError),
    Disconnect,
    Shutdown,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ServerError {
    TooManyPlayers,
    //TODO: InvalidAlias,
}
